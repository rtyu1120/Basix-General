# Basix General
This project is made to easily move, deploy my self-hosted environment.

## Deployment Guide
1. Configure your ZNC server by this command: `docker run -it --rm -v znc-cfg:/znc-data znc --makeconf`. You should make it listen to `6697` port.
2. Edit your `cloudflare.ini` to your credentials to issue a wildcard certificate.
3. Issue your Let's Encrypt certificate by this command: `certbot certonly -a dns-cloudflare --dns-cloudflare-credentials ./cloudflare.ini  -i nginx -d "*.basix.tech" -d basix.tech --server https://acme-v02.api.letsencrypt.org/directory`.
4. Run node_exporter directly on your server. If you don't want to do this, remove the `system` section in `prometheus.yml`
5. `docker-compose up -d`

## Note
* In this project, I assumed you would deploy this to basix.tech. Please replace this if you are going to deploy this elsewhere.

## Wishlist
* wallabag